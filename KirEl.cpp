﻿//KirEl

#include <iostream>
#include <fstream>
#include <cmath>
#include <complex>
#include <string>

#define pause system("pause")
#define Complex complex<double>

#define PI 3.141593
#define EPS 1e-7

#define MAX_PHI_0 16
#define MAX_ANG_VALUES MAX_PHI_0*8

using std::cout;
using std::complex;
using std::endl;
using std::string;
using std::fstream;
using std::ofstream;
using std::ios;



struct arr {
	double* data;
	int n;
};

struct arr2d {
	arr* data;
};

void progNameOut() {
	cout << endl << endl;
	cout << "¶¶``¶¶`¶¶¶¶¶¶`¶¶¶¶¶``¶¶¶¶¶``¶¶````````````¶¶``¶¶``¶¶¶¶`````````¶¶" << endl;
	cout << "¶¶`¶¶````¶¶```¶¶``¶¶`¶¶`````¶¶````````````¶¶``¶¶`¶¶``¶¶``````¶¶¶¶" << endl;
	cout << "¶¶¶¶`````¶¶```¶¶¶¶¶``¶¶¶¶```¶¶`````¶¶¶¶¶``¶¶``¶¶````¶¶`````````¶¶" << endl;
	cout << "¶¶`¶¶````¶¶```¶¶``¶¶`¶¶`````¶¶`````````````¶¶¶¶```¶¶```````````¶¶" << endl;
	cout << "¶¶``¶¶`¶¶¶¶¶¶`¶¶``¶¶`¶¶¶¶¶``¶¶¶¶¶¶``````````¶¶```¶¶¶¶¶¶`¶¶`````¶¶" << endl;
	cout << endl << endl;
}

bool isEqual(double x, double y) { return abs(x - y) <= EPS; }

double toRad(double deg, char mode) { //converting degrees into radians
	if (mode == 'd') {
		return deg*PI / 180;
	}
	if (mode == 'f') {
		double a, b, tmp, res;
		b = modf(deg, &a)*1e2;
		return (a + b / 60)*PI / 180;
	}
}

double* findF(double DELTA, double PSI, double n_0, double n_1, double n_2, double k_2,
	double phi_0, char mode) {
	Complex i(0, 1);

	double phi_1 = asin(sin(phi_0)*n_0 / n_1);

	double eps_0 = pow(n_0, 2);
	double eps_1 = pow(n_1, 2);
	Complex eps_2 = pow((n_2 - i*k_2), (Complex)2);

	double U_0s = sqrt(eps_0)*cos(phi_0);
	double U_0p = sqrt(eps_0) / cos(phi_0);
	double U_1s = sqrt(eps_1 - eps_0*pow(sin(phi_0), 2));
	double U_1p = eps_1 / sqrt(eps_1 - eps_0*pow(sin(phi_0), 2));
	Complex U_2s = sqrt(eps_2 - eps_0*pow(sin(phi_0), 2));
	Complex U_2p = eps_2 / sqrt(eps_2 - eps_0*pow(sin(phi_0), 2));

	Complex R_01s = (U_0s - U_1s) / (U_0s + U_1s);
	Complex R_01p = -(U_0p - U_1p) / (U_0p + U_1p);
	Complex R_12s = (U_1s - U_2s) / (U_1s + U_2s);
	Complex R_12p = -(U_1p - U_2p) / (U_1p + U_2p);

	Complex rho = tan(PSI)*exp(i*DELTA);

	Complex A = rho*R_01p*R_12p*R_12s - R_01s*R_12p*R_12s;
	Complex B = rho*(R_12s + R_01p*R_01s*R_12p) - R_01p*R_01s*R_12s - R_12p;
	Complex C = rho*R_01s - R_01p;

	Complex X1 = (-B + sqrt(pow(B, 2) - (Complex)4 * A*C)) / ((Complex)2 * A);
	Complex X2 = (-B - sqrt(pow(B, 2) - (Complex)4 * A*C)) / ((Complex)2 * A);

	if (mode == 'a') {
		double* ret = new double[2];
		ret[0] = (abs(X1) - 1);
		ret[1] = (abs(X2) - 1);
		return ret;
	}

	if (mode == 'b') {
		double* ret = new double[4];
		ret[0] = X1.real();
		ret[1] = X1.imag();
		ret[2] = X2.real();
		ret[3] = X2.imag();
		return ret;
	}

}

double findDelta(double* retF) {
	return abs(retF[1] - retF[0]);
}

Complex mainAlg(double* n1Ans, double DELTA, double PSI, double phi_0, double lambda,
	double n_0, double n_2, double k_2, double n_1min, double n_1max, double delta_n_1) {
	Complex i(0, 1);

	double delta_F, delta_F_, n_1;
	double* retF;
	n_1 = n_1min;

	delta_F_ = findDelta(findF(DELTA, PSI, n_0, n_1, n_2, k_2, phi_0, 'a'));
	n_1 += delta_n_1;

	while ((delta_F = findDelta(findF(DELTA, PSI, n_0, n_1, n_2, k_2, phi_0, 'a'))) < delta_F_ && n_1 < n_1max) {
		n_1 += delta_n_1;
		delta_F_ = delta_F;
	}

	if (n_1 >= (n_1min + delta_n_1 - delta_n_1 / 2) && n_1 <= (n_1min + delta_n_1 + delta_n_1 / 2)) {
		cout << "enlarge n1 interval (min)" << endl;
		exit(1);
	}
	if (n_1 >= (n_1max - delta_n_1 / 2) && n_1 <= (n_1max + delta_n_1 / 2)) {
		cout << "enlarge n1 interval (max)" << endl;
		exit(1);
	}

	n_1 -= delta_n_1;
	*n1Ans = n_1;
	double phi_1 = asin(sin(phi_0)*n_0 / n_1);
	retF = findF(DELTA, PSI, n_0, n_1, n_2, k_2, phi_0, 'b');

	Complex X1 = retF[0] + i*retF[1];
	Complex X2 = retF[2] + i*retF[3];

	Complex d1 = log(X1)*lambda / -((Complex)4 * i*PI*cos(phi_1));
	Complex d2 = log(X2)*lambda / -((Complex)4 * i*PI*cos(phi_1));

	return d1.imag() < d2.imag() ? d1 : d2;
}

int main(int argc, char** argv) {

	//----starting
	if (argc == 1) {
		cout << "no file chosen -- sample.txt will be created" << endl;
		ofstream genFile("sample.txt");
		if (!genFile) {
			cout << "some problems with creating the sample file. Try to run as admin" << endl;
			return 1;
		}
		cout << ". . ." << endl;
		genFile << "mode: X (d - all angles in degrees; f - all angles in d.mm format)\n\n";
		genFile << "(HOW TO ENTER ANGLES IN d.mm FORMAT: 81°4' -> 81.04; 150°45' -> 150.45)\n";
		genFile << "(you can also use 'comma' instead 'point' to separate the whole and fractional parts)\n\n";
		genFile << "Adjusting_parameters[angle]:\n";
		genFile << "P0\t\tA0\t\tC0\n";
		genFile << "XX\t\tXX\t\tXX\n\n";
		genFile << "laser_wave_length[nm]: 	XX\n\n";
		genFile << "sample_parameters:\n";
		genFile << "\t\tn0:	XX\n";
		genFile << "\t\tn2:	XX\n";
		genFile << "\t\tk2:	XX\n\n";
		genFile << "search_range_n1:\n";
		genFile << "\tfrom XX to XX step 0 (0 - for automatic calculation) XX steps\n\n";
		genFile << "\t\tI\t\t\t\tII\t\t\t\tIII\t\t\t\tIV\n";
		genFile << "phi_0\tP1\t\tA1\t\tP2\t\tA2\t\tP3\t\tA3\t\tP4\t\tA4\n\n";

		genFile << "60\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n";
		genFile << "70\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n\n";
		genFile << "60\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n";
		genFile << "70\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n\n";
		genFile << "60\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n";
		genFile << "70\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n\n";
		genFile << "60\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n";
		genFile << "70\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n\n";
		genFile << "60\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n";
		genFile << "70\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\t\txxx\n\n";

		genFile.close();
		cout << "sample.txt was created, you should rename and fill it" << endl;

		pause;
		return 0;
	}
	
	if (argc > 2) {
		cout << "to much arguments!" << endl;
		return 1;
	}

	//-----------------------------------------------
	progNameOut();

	char c;
	double d;
	string str;
	double P0, A0, C0;

	//-----------------------------initialisation
	Complex* d_answers = new Complex[MAX_ANG_VALUES / 8];
	double* n1_answers = new double[MAX_ANG_VALUES / 8];

	arr Ree; Ree.n = 0; Ree.data = new double[MAX_PHI_0];
	arr Delta; Delta.n = 0; Delta.data = new double[MAX_ANG_VALUES / 8];
	arr Psi; Psi.n = 0; Psi.data = new double[MAX_ANG_VALUES / 8];
	arr2d Main; Main.data = new arr[MAX_PHI_0];

	for (int i = 0; i < MAX_PHI_0; i++) {
		Main.data[i].data = new double[MAX_ANG_VALUES];
		Main.data[i].n = 0;
	}
	
	//-----------------------------opening file
	fstream input(argv[1], ios::in | ios::out | ios::binary);
	cout << "your file: " << argv[1] << endl << endl;
	if (!input) { cout << "error while reading the file! - - - #1" << endl; pause; return 1; } //check opening the file
	
	//-----------------------------changing all ',' to '.'
	while (input >> c) { 
		if (c == ',') {
			int t = input.tellg();
			input.seekp(--t, ios::beg);
			input << '.';
		}
	}

	//-----------------------------reset file pointer
	input.clear();
	input.seekg(0);

	//-----------------------------------------------------------------start parsing
	input >> str;
	char mode; input >> mode;

	for (int i = 0; i < 44; i++)
		input >> str;

	input >> P0 >> A0 >> C0;

	input >> str;
	double lambda; input >> lambda;

	double n_0, n_2, k_2;
	input >> str;

	input >> str;
	input >> n_0;

	input >> str;
	input >> n_2;

	input >> str;
	input >> k_2;

	input >> str;
	input >> str;

	double n_1min, n_1max;
	input >> n_1min;
	input >> str;
	input >> n_1max;

	input >> str;
	double step; input >> step;
	double n_steps;
	int step_mode = 1;
	if (!isEqual(step, 0))
		for (int i = 0; i < 7; i++)
			input >> str;
	else {
		step_mode = 0;
		for (int i = 0; i < 5; i++)
			input >> str;
		input >> n_steps;
		input >> str;
		step = (n_1max - n_1min) / n_steps;
	}

	for (int i = 0; i < 13; i++)
		input >> str;

	int i = 0, current;

	while (input >> d) { //main data parsing
		if (!(i % 9)) {
			current = -1;
			for (int j = 0; j < Ree.n; j++)
				if (isEqual(d, Ree.data[j]))
					current = j;
			if (current == -1) {
				Ree.data[Ree.n] = d;
				current = Ree.n;
				Ree.n++;
			}
		}
		else {
			Main.data[current].data[Main.data[current].n] = d;
			Main.data[current].n++;
		}
		i++;
	}
	input.close();
	if (i % 9) { cout << "all lines must be filled! - - - #3" << endl; pause; return 1; }
	//-------------------------------------------------------------------------------------end parsing


	//------------------------------print info
	cout << "mode: " << mode << endl;
	cout << "P0 = " << P0 << "  A0 = " << A0 << "  C0 = " << C0 << endl;
	cout << "laser wave length [nm]: " << lambda << endl;
	cout << "sample parameters:  " << "n0 = " << n_0 << "  n2 = " << n_2 << "  k2 = " << k_2 << endl;
	cout << "search n1 range:  " << n_1min << " ... " << n_1max;
	if (step_mode)
		cout << "  step = " << step << endl;
	else {
		cout << "  step = " << step << " (" << n_steps << " steps)" << endl;
	}
	cout << endl;

	//-----------------------------converting into radians
	P0 = toRad(P0, mode); A0 = toRad(A0, mode); C0 = toRad(C0, mode);
	for (int i = 0; i < Ree.n; i++) {
		Ree.data[i] = toRad(Ree.data[i], mode);
		for (int j = 0; j < Main.data[i].n; j++)
			Main.data[i].data[j] = toRad(Main.data[i].data[j], mode);
	}

	//-----------------------------calculating
	//----------averaging
	for (int i = 0; i < Ree.n; i++) {
		double tmp;
		Delta.n = 0;
		Psi.n = 0;
		for (int j = 0; j < Main.data[i].n; j += 8) {
			tmp = (-Main.data[i].data[j] - Main.data[i].data[j + 2] + Main.data[i].data[j + 4] + Main.data[i].data[j + 6]) / 2;
			while (tmp < 0)
				tmp += PI;
			Delta.data[i] += tmp;
			Delta.n++;
			tmp = (abs(Main.data[i].data[j + 1] - Main.data[i].data[j + 3]) + abs(Main.data[i].data[j + 5] - Main.data[i].data[j + 7])) / 4;
			Psi.data[i] += tmp;
			Psi.n++;
		}
		Delta.data[i] /= Delta.n;
		Psi.data[i] /= Psi.n;
	}

	//----------finding n, d
	for (int i = 0; i < Ree.n; i++) {
		d_answers[i] = mainAlg(&(n1_answers[i]), Delta.data[i], Psi.data[i], Ree.data[i],
			lambda, n_0, n_2, k_2, n_1min, n_1max, step);
	}


	//-----------------------------printing result
	cout << "Result:" << endl;
	for (int i = 0; i < Ree.n; i++)
		cout << Ree.data[i] * 180 / PI << " deg.:    n1 = " << n1_answers[i] << "    d = "
		<< d_answers[i].real() << " +- " << abs(d_answers[i].imag()) << " nm" << endl;
	cout << endl;

	pause;
	return 0;
}